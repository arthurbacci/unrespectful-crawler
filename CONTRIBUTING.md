# Contributing

If you want to contribute with something, please make a pull request

To make a pull request, follow the steps:

- Make a fork of this repository

- Clone your fork with

```
$ git clone https://gitlab.com/YOUR_USERNAME/unrespectful-crawler.git
$ cd unrespectful-crawler
```

- Edit what you want

- Now, add, commit and push

```
$ git add .
$ git commit -m "COMMIT_MESSAGE"
$ git push
```

- Go to your forked repository in Gitlab and make the pull request

## Thanks for contributing!
