# Unrespectful crawler

A crawler that does not respect `robots.txt`, made in NodeJS.

## Dependencies

You will need only `git`, `nodejs` and `npm`.

### Parabola GNU/Linux-libre, Arch Linux, Manjaro Linux...

```
# pacman -S git nodejs npm
```

### Debian GNU/Linux, Ubuntu, Linux Mint...

```
# apt install git nodejs npm
```

## Running

First:

```
$ git clone https://gitlab.com/arthurbacci/unrespectful-crawler/
$ cd unrespectful-crawler
$ npm install
```

And to run:

```
$ npm start
```

## Contributing

If you want to contribute, please read `CONTRIBUTING.md` ;D
