const axios = require('axios');
const url = require('url');
const fs = require('fs');

const start_point = 'http://gnu.org/';
const access_limit = 10000;

let links = [start_point];
let links_i = 0;

function join_urls(_url) {
    let link = links[links_i];
    if (_url.match('https{0,1}://')) {
	return _url;
    }
    if (_url === '') {
	return link;
    }
    if (_url[0] === '/') {
	return new URL(_url, link).href;
    }
    if (link[link.length - 1] == '/') {
	return link + url;
    }
    return link + '/' + _url;
}

function clear_link(el) {
    let el_url = new URL(el);
    el_url.protocol = 'http:';
    el_url.href = el_url.origin + el_url.pathname;
    return el_url.href;
}

links = links.map(clear_link);

async function keep_trying(callback) {
    async function dontgiveup() {
	try {
	    callback();
	} catch {
	    await new Promise(resolve => setTimeout(resolve, 100));
	    dontgiveup();
	}
    }
    dontgiveup();
}

function try_x_times(callback, x) {
    if (x > 0) {
	try {
	    callback();
	} catch {
	    try_x_times(callback, x - 1);
	}
	return 1;
    } else {
	return 0;
    }
}

let at_promise = 0;
let not_exist = [];

function iteration(_url, i) {
    let promise = new Promise(function (resolve, reject) {
	
	axios.get(_url).then(res => {
	    keep_trying(() => {
		console.log(`${at_promise + 1} / ${links.length - 1} | ${i}`);
		at_promise += 1;
	    });
	    let new_links = res.data.match(/(?<=href\s*=\s*["'])([^"'#]*)/g).map(join_urls);
	    new_links = new_links.map(clear_link);
	    keep_trying(() => {
		links = [...new Set([...links, ...new_links])];
	    });
	    fs.writeFile(`pages/${_url.replace(/\//g, '?')}`, res.data, () => 0);
	    resolve();
	}).catch(err => {
	    keep_trying(() => {
		console.log(`${at_promise + 1} / ${links.length - 1} | ${i} | err`);
		at_promise += 1;
	    });
	    keep_trying(() => not_exist.push(i));
	    reject();
	});
    });
    return promise;
}

async function main() {
    
    await axios.get(links[0]).then(res => {
	let new_links = res.data.match(/(?<=href\s*=\s*["'])([^"'#]*)/g).map(join_urls);
	new_links = new_links.map(clear_link);
	links = [...new Set([...links, ...new_links])];
	console.log(links.join('\n'));
    }).catch(err => {
	console.log('Error requesting the start point, error:');
	console.log(err);
    });
    
    for (links_i = 1; links_i < links.length && links_i < access_limit; links_i++) {
	iteration(links[links_i], links_i).then(() => null, () => null);
	await new Promise(resolve => setTimeout(resolve, 100));
	if (links_i % 100 == 0) {
	    fs.writeFile('links.txt', links.join('\n'), () => console.log('ALL LINKS WRITTEN ON links.txt FILKE'));
	}
    }

    while (at_promise < access_limit) {
	await new Promise(resolve => setTimeout(resolve, 1000));
    }
    
    console.log('Not exist:', not_exist.join(' '));
    fs.writeFile('links.txt', links.join('\n'), () => console.log('ALL LINKS WRITTEN ON links.txt FILE'));
}

main();

